# Personal Website
This is a repo for my [personal website](https://ked.wtf/), built using the [11ty](https://www.11ty.dev/) static site generator.

## Build
Clone the repo and run `npm install`. Once dependencies are installed, run `npm run build`. The built files will now be in the `public` directory.